name := "plr-edit"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies ++= Seq(
    "org.scodec" %% "scodec-core" % "1.11.7",
    "org.scodec" %% "scodec-bits" % "1.1.20",
    "org.scodec" %% "scodec-stream" % "2.0.0",
    "org.typelevel" %% "cats-effect" % "2.1.3",
    "org.typelevel" %% "cats-core" % "2.1.1",
    "co.fs2" %% "fs2-core" % "2.4.2",
    "co.fs2" %% "fs2-io" % "2.4.2",
    "tf.bug" %% "nose" % "0.2.0",
)
