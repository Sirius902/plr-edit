package com.gitlab.sirius902.plr_edit

import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

object AES128 {
  lazy val cipher: Cipher = Cipher.getInstance("AES/CBC/NoPadding")

  def encrypt(data: Array[Byte], key: Array[Byte], iv: Array[Byte]): Array[Byte] = {
    cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(iv))
    cipher.doFinal(data)
  }

  def decrypt(data: Array[Byte], key: Array[Byte], iv: Array[Byte]): Array[Byte] = {
    cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(iv))
    cipher.doFinal(data)
  }
}
