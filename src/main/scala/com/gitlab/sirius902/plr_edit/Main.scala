package com.gitlab.sirius902.plr_edit

import java.nio.file.{Files, Paths}

import com.gitlab.sirius902.plr_edit.Terraria.encryptionKey
import scodec.bits.BitVector

object Main {
  def main(args: Array[String]): Unit = {
    val home = System.getProperty("user.home")
    val encrypted = Files.readAllBytes(Paths.get(home, "Desktop/plr/Players/Poo.plr"))
    val decrypted = AES128.decrypt(encrypted, encryptionKey, encryptionKey)

    val playerFile = Codecs.playerFile.decodeValue(BitVector(decrypted))
    val reEncoded = playerFile.flatMap(Codecs.playerFile.encode)

    reEncoded.map { plr =>
      Files.write(Paths.get(home, "Desktop/plr/Poo.plr"), encrypted)
      Files.write(Paths.get(home, "Desktop/plr/Poo.test.plr"), AES128.encrypt(plr.toByteArray, encryptionKey, encryptionKey))
    }
  }
}
