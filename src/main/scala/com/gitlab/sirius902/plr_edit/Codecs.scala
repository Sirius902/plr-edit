package com.gitlab.sirius902.plr_edit

import java.nio.charset.Charset

import com.gitlab.sirius902.plr_edit.Terraria.{FileMetadata, FileType, PlayerFile}
import scodec.bits._
import scodec.codecs._
import scodec.{Attempt, Codec, DecodeResult, Decoder, Err, SizeBound}
import tf.bug.nose.space.rgb.StandardRGB

import scala.annotation.tailrec

object Codecs {
  lazy val varIntL: Codec[Int] = new Codec[Int] {
    override def encode(value: Int): Attempt[BitVector] = {
      @tailrec
      def go(in: BitVector, out: BitVector): Attempt[(BitVector, BitVector)] =
        if (in.dropRight(7).containsSlice(BitVector.one))
          go(in.dropRight(7), out ++ (true +: in.takeRight(7)))
        else
          Attempt.successful(in, out)

      for {
        bits <- Attempt.successful(BitVector.fromInt(value))
        r <- go(bits, BitVector.empty)
        (remaining, result) = r
        padded = if (remaining.length >= 8) remaining else remaining.padLeft(8)
      } yield result ++ padded.takeRight(8)
    }

    override def decode(bits: BitVector): Attempt[DecodeResult[Int]] = {
      def stepVarInt(agg: Int, i: Int): Decoder[Int] = byte.flatMap { read =>
        val value = read & 0x7F
        val next = agg | (value << (7 * i))

        if ((read & 0x80) == 0) Decoder.point(next)
        else if (i >= 4) Decoder.liftAttempt(Attempt.failure(Err("varIntL is too big")))
        else stepVarInt(next, i + 1)
      }

      stepVarInt(0, 0).decode(bits)
    }

    override def sizeBound: SizeBound = SizeBound.bounded(1, 5)
  }

  lazy val dotnetString: Codec[String] = variableSizeBytes(varIntL, string(Charset.forName("UTF-8")))

  lazy val colorValueF: Codec[Double] =
    uint8L.xmap(n => n.toDouble / 255.0, f => (f * 255.0).toInt)

  lazy val dotnetRGB: Codec[StandardRGB] = (colorValueF :: colorValueF :: colorValueF).as[StandardRGB]

  // TODO: Support multiple releases?
  lazy val release: Codec[Unit] = int32L.unit(194)

  lazy val fileMagic: Codec[Unit] = constant(ByteVector(0x72, 0x65, 0x6C, 0x6F, 0x67, 0x69, 0x63))

  lazy val fileType: Codec[FileType] = uint8L.exmap({
    case 0 => Attempt.successful(FileType.None)
    case 1 => Attempt.successful(FileType.Map)
    case 2 => Attempt.successful(FileType.World)
    case 3 => Attempt.successful(FileType.Player)
    case _ => Attempt.failure(Err("Invalid file type"))
  }, {
    case FileType.None => Attempt.successful(0)
    case FileType.Map => Attempt.successful(1)
    case FileType.World => Attempt.successful(2)
    case FileType.Player => Attempt.successful(3)
  })

  def fileMetadata(expectedType: FileType): Codec[FileMetadata] =
    (
      fileMagic ~>
        fileType.exmap({
          case t if t == expectedType => Attempt.successful(t)
          case _ => Attempt.failure(Err("Unexpected file type"))
        }, Attempt.successful[FileType]) ::
        uint32L ::
        (ignore(63) ~> bool)
      ).as[FileMetadata]

  lazy val playerFile: Codec[PlayerFile] =
    (
      release ~>
        ("metadata" | fileMetadata(FileType.Player)) ::
        ("name" | dotnetString) ::
        ("difficulty" | uint8L) ::
        ("playTime" | int64L) ::
        ("hair" | int32L) ::
        ("hairDye" | uint8L) ::
        ("hideVisual" | (uint8L ~ uint8L)) ::
        ("hideMisc" | uint8L) ::
        ("skinVariant" | uint8L) ::
        ("statLife" | int32L) ::
        ("statLifeMax" | int32L) ::
        ("statMana" | int32L) ::
        ("statManaMax" | int32L) ::
        ("extraAccessory" | (ignore(7) ~> bool)) ::
        ("downedDD2EventAnyDifficulty" | (ignore(7) ~> bool)) ::
        ("taxMoney" | int32L) ::
        ("hairColor" | dotnetRGB) ::
        ("skinColor" | dotnetRGB) ::
        ("eyeColor" | dotnetRGB) ::
        ("shirtColor" | dotnetRGB) ::
        ("underShirtColor" | dotnetRGB) ::
        ("pantsColor" | dotnetRGB) ::
        ("shoeColor" | dotnetRGB) ::
        ("armor" | vectorOfN(provide(20), (int32L :: uint8L).as[(Int, Int)])) ::
        ("dye" | vectorOfN(provide(10), (int32L :: uint8L).as[(Int, Int)])) ::
        ("inventory" | vectorOfN(provide(58), (int32L :: int32L :: uint8L :: (ignore(7) ~> bool)).as[(Int, Int, Int, Boolean)])) ::
        bytes
      ).as[PlayerFile]
}
