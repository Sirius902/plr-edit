package com.gitlab.sirius902.plr_edit

import scodec.bits.ByteVector
import tf.bug.nose.space.rgb.StandardRGB

object Terraria {
  val encryptionKey: Array[Byte] = Array(0x68, 0x00, 0x33, 0x00, 0x79, 0x00, 0x5F, 0x00, 0x67, 0x00, 0x55, 0x00, 0x79, 0x00, 0x5A, 0x00)

  sealed trait FileType

  object FileType {

    case object None extends FileType

    case object Map extends FileType

    case object World extends FileType

    case object Player extends FileType

  }

  case class FileMetadata(fileType: FileType, revision: Long, isFavorite: Boolean)

  object FileMetadata {
    def fromCurrentSettings(t: FileType): FileMetadata = FileMetadata(t, 0, isFavorite = false)
  }

  case class PlayerFile(
                         metadata: FileMetadata,
                         name: String,
                         difficulty: Int,
                         playTime: Long,
                         hair: Int,
                         hairDye: Int,
                         hideVisual: (Int, Int),
                         hideMisc: Int,
                         skinVariant: Int,
                         statLife: Int,
                         statLifeMax: Int,
                         statMana: Int,
                         statManaMax: Int,
                         extraAccessory: Boolean,
                         downedDD2EventAnyDifficulty: Boolean,
                         taxMoney: Int,
                         hairColor: StandardRGB,
                         skinColor: StandardRGB,
                         eyeColor: StandardRGB,
                         shirtColor: StandardRGB,
                         underShirtColor: StandardRGB,
                         pantsColor: StandardRGB,
                         shoeColor: StandardRGB,
                         /// (type, pre)
                         armor: Vector[(Int, Int)],
                         /// (type, pre)
                         dye: Vector[(Int, Int)],
                         /// (type, stack, pre, favorited)
                         inventory: Vector[(Int, Int, Int, Boolean)],
                         unknown: ByteVector,
                       )

}
